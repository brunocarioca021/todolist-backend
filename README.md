# todolist-backend
Projeto Final Lista de Tarefas.

A ideia desse projeto é o desenvolvimento de uma lista de tarefas.


A aplicação deve ser criada em repositório separados entre front-end e backend de acordo com as informações para facilitar o deploy.


Backend 


Estrutura da Entidade: Tarefas
Título
Descrição
Prioridade (Alta, Média e Baixa)
Status (Fazer, Fazendo, Feito)
Prazo
Data de criação
Endpoints 
[GET] Leitura de todas as tarefas
[GET] Leitura de tarefas individuais (por ID) 
[POST] Criação de tarefas 
[PUT] Edição de tarefas por ID 
[DELETE] Exclusão de tarefas por ID 
Requisitos 
Validação de dados em todos os endpoints 
Status Code corretos em todos os endpoints 
200, 201, 400, 404, etc
Persistência de Dados no MongoDB 
Formatação do código utilizando o Prettier 
Exportar os arquivos de requisição 
Deploy do projeto no Heroku 
Deploy do banco de dados na Cloud Atlas 
